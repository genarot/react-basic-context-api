# React Context API basic - LyricFinder

Este proyecto **React**.  Se realizo para probar el API Context.

`No esta realizado con las mejores practicas.`

## Desarrollado con:
* [React js](https://reactjs.org/docs/getting-started.html)
* [Create-react-app](https://github.com/facebook/create-react-app) 
* [Yarn](https://yarnpkg.com/en/)

## Instalaciòn
`yarn install`

## Desplegar SPA
`yarn start`

## Autor
* **Genaro Tinoco** - AtomicDev  - [genarot](https://gitlab.com/genarot)