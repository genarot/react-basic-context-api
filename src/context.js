import React, { Component } from 'react';
import Axios from 'axios';

const Context = React.createContext();
const reducer = ( state, action ) => {
    switch( action.type ) {
        case 'SEARCH_TRACKS': return {...state, track_list: [], heading: 'Searching...', searching: true}
        case 'SEARCH_TRACKS_SUCCESS': return {...state, track_list: action.payload, heading: `Search Result for: "${action.searchText}"`, searching: false}
        default: return state;
    }
}

export class Provider extends Component {
    state = {
        track_list: [],
        heading:'Top 10 Tracks',
        searching: true,
        dispatch: action => this.setState(state => reducer(state, action))
    }
    componentDidMount() {
        Axios.get(`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/chart.tracks.get?page=1&page_size=10&country=us&f_has_lyrics=1&apikey=${process.env.REACT_APP_MM_KEY}`)
        .then(res=> {
            this.setState({track_list: res.data.message.body.track_list, searching: false})
        })
        .catch(err=>console.error(err))
    }

    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        );
    }
}

export const Consumer = Context.Consumer;