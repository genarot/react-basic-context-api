import React, { Component } from 'react';
import axios from 'axios';
import {Consumer} from '../../context';

class Search extends Component {
    state = {
        trackTitle: ''
    }

    handleChange = ( evt ) => {
        this.setState({[evt.target.name]: evt.target.value})
    }

    handleSubmit( dispatch, evt) {
        evt.preventDefault();
        dispatch({type:'SEARCH_TRACKS'})
        axios.get(`https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.search?q_track=${this.state.trackTitle}&page=1&page_size=10&if_has_lyrics=1&s_track_rating=desc&apikey=${process.env.REACT_APP_MM_KEY}`)
        .then(res=> {
            dispatch({type: 'SEARCH_TRACKS_SUCCESS', payload: res.data.message.body.track_list, searchText: this.state.trackTitle})
            this.setState({trackTitle: ''})
        })
        .catch(err=>console.error(err))
    }

    render() {
        return (
            <Consumer>
                {
                    value => {
                        const {dispatch} = value;
                        return (
                            <div className="card card-body mb-4 p-4">
                                <h1 className="display-4 text-center">
                                    <i className="fas fa-music"></i>Search For A Song
                                </h1>
                                <p className="lead text-center">Get the Lyrics for any Song</p>
                                <form onSubmit={this.handleSubmit.bind(this, dispatch)}>
                                    <div className="form-group">
                                        <input 
                                            type="tex" 
                                            className="form-control form-control-lg" 
                                            placeholder="Song Title..."
                                            name="trackTitle"
                                            value={this.state.trackTitle}
                                            onChange={this.handleChange}
                                            />
                                    </div>
                                    <button className="btn btn-primary btn-lg btn-block mb-5" type="submit">
                                        Get Track Lyrics
                                    </button>
                                </form>
                            </div>
                        )
                    }
                }
            </Consumer>
        );
    }
}

export default Search;